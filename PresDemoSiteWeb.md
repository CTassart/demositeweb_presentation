---
title: " "
output:
  ioslides_presentation:
    fig_caption: no
    keep_md: yes
    smaller: yes
    widescreen: yes
---





# Communauté R - 12/10/2021

<br>

Créer un site Web statique, avec `R`
 
Le déployer, avec `Gitlab`


# Intro


<details>
<summary markdown="span">**Kesako ?**</summary>

**Un site web statique, c'est :**  

- un ensemble de pages et de ressources reliées par des hyperliens  
- développé à l'aide de langages de programmation web,  
- hébergé sur un serveur web   

</details>

<details>
<summary markdown="span">**Comment ?**</summary>

- Le `HTML` permet d'écrire le contenu de sa page([Exemple](http://www.softastuces.com/tuto/site/html/ex.php)) ;  
  - Le `markdown` est un langage offrant une syntaxe facile à lire et à écrire, permettant de générer (entre autre) du `HTML`.  
- Le `CSS` permet de mettre en page le contenu (emplacement des éléments), et mettre en forme le texte (police d'écriture, couleur, taille).  

</details>


<details>
<summary markdown="span">**Avec R ?**</summary>
- **Quelques exemples :**

  - [Cette présentation (et +) en mode site Web](https://ctassart.gitlab.io/demositeweb/) ([sources](https://gitlab.com/CTassart/demositeweb))  
  - [Lucy D'Agostino Mc Gowan... statisticienne](https://www.lucymcgowan.com/)  
  - [Nick's website](http://nickstrayer.me/personal_site/index.html)  
  - [Emily Zabor : son package `ezfun`](http://www.emilyzabor.com/ezfun/)  
  - [Un exemple **Insee** : le Pôle EEC]()  
  - [Un exemple moins sérieux : un site de tennis de table... mais avec du CSS et des images](https://ctassart.github.io/beauvaloiTT/index.html) ([Code source](https://github.com/CTassart/beauvaloiTT))  

</details>

<details>
<summary markdown="span">**Pourquoi ? Pour faire quoi ? A l'insee ?**</summary>

**Partager des informations, des données, des résultats**

- [x] Dans un SAR  
- [x] Dans un SED  
- [x] Dans un SES  
  - [x] Pôle EEC  
  - [x] [site Tourisme (vite bricolé - des données fictives)](http://t19qhl.gitlab-pages.insee.fr/granturismo/index.html)  
</details>

## Objectifs

- Construire un site Web statique simple... puis l'améliorer
- L'héberger sur `gitlab` (et `github`) pour que le monde entier puisse y accéder,
- Savoir comment demander de l'aide à votre ami `Google` pour l'améliorer.


## Plan

- Construire un site Web, **avec `RStudio`**  
- Intégrer du **code `R`**  
- Déployer le site sur un serveur **(`gitlab` et/ou `Github`)**  
- Une démo... live :(

<img src="../demositeweb_presentation/images/pasPeur.jpg" width="200" height="200" />

## Prérequis

|   |   |   |
|---|---|---|
|Git / Gitlab / Github | **un peu... (un peu beaucoup)**  |[Lien vers la formation `R Collaboratif`](https://linogaliana.gitlab.io/collaboratif/git.html) |
|R Markdown|**beaucoup... (mais pas trop)**||
|RStudio|**passionnément**||
|R|**A la folie... (éventuellement)**||
|CSS, HTML|**pas du tout... (OK... un petit peu alors)**||


<details>
<summary markdown="span" class="test2">**Attention !**</summary>

`install.packages("rmarkdown")`


</details>




## Euuuh ! On commence par quoi ?

- [ ] On construit le site puis on le déploie ?
- [X] On "déploie" le site avant de le construire ?


# 1. Créer son dépôt distant sur `Gitlab`


## [Gitlab externe](gitlab.com) ou [Gitlab interne Insee](https://gitlab.insee.fr)

- **URL** : 
  -  [Gitlab externe (https://about.gitlab.com/)](https://about.gitlab.com/)
  -  [Gitlab interne Insee (https://gitlab.insee.fr)](https://gitlab.insee.fr)

![](../demositeweb_presentation/images/3_createRepo.png)

---

![](../demositeweb_presentation/images/4_NewRepo.png)

---

![](../demositeweb_presentation/images/5_ProjectName.png)

---

## Copier l'URL

- Bouton `Clone`  
  - `Clone with HTTPS`  

![](../demositeweb_presentation/images/3_cloneBouton.png)

On va tout de suite se servir de ce lien, dans RStudio.


# 2. Créer un projet RStudio (Version Control)

---

## New Project
- `Version Control`
    - `Git`
        - `Clone Git repository`
            
            
![](../demositeweb_presentation/images/1_versionControl.png)

---

![](../demositeweb_presentation/images/2_git.png)

---

![](../demositeweb_presentation/images/3_clone.png)

 
# 3. Les fichiers


## Créer de nouveaux fichiers

On doit créer plusieurs fichiers :  

<img src="../demositeweb_presentation/images/9_newFile.png" width="500" height="500" />

---

**Quels types de fichiers ?**

|Type fichiers|Extension|objectifs|Nb|
|---|---|---|---|
|`R Markdown...`|.Rmd|contenu des pages web|1 par page de contenu|
|`Text File`|.yml|fichiers de paramétrages|2|
|`CSS File`|.css|pour le style global du site|1|

## Les fichiers indispensables

**Quels fichiers ?**

|nom du fichier|objectif||
|---|---|---|
|`_site.yml`|métadonnées pour le site Web|Organise la disposition des pages, la barre de navigation... |
|`.gitlab-ci.yml`|permet le déploiement sur [`gitlab`](https://gitlab.com/)| Inutile pour [`github`](https://github.com/)  |
|`index.Rmd`|contenu de la page d'accueil du site| `Communauté R`|

**C'est suffisant pour créer un site web !**  

||||
|---|---|---|
|`about.Rmd`|contenu de la page **A propos** | `Communauté R`|
|`page2.Rmd`|contenu de la page **Page 2** | `Page 2`|
|`page3.Rmd`|contenu de la page **Page 3** | `Page 3`|


---

### `index.Rmd`

```
---
title: "Communauté R"
---


Créer un site Web, avec `R`
 
Le déployer, avec `Gitlab`


```

---


### `about.Rmd`

```
---
title: "A propos"
---


**Introduction**

- Construire un site Web statique simple
- L'héberger sur `gitlab` (et `github`) pour que le monde entier puisse y accéder,
- Savoir comment demander de l'aide à votre ami `Google` pour l'améliorer.

---


**Plan**

- Construire un site Web, avec RStudio
- Intégrer du code R
- Déployer le site sur un serveur (`gitlab` et/ou `Github`)
- Quelques exemples, pour aller plus loin...
  - Markdown
  - Styliser son site (CSS...)
  - Ressources


---

**Prérequis**

|   |   |   |
|---|---|---|
|Git / Gitlab / Github | **un peu... (un peu beaucoup)**  |[Lien vers la formation `R Collaboratif`](https://linogaliana.gitlab.io/collaboratif/git.html) |
|R Markdown|**beaucoup... (mais pas trop)**||
|RStudio|**passionnément**||
|R|**A la folie... (éventuellement)**||
|CSS, HTML|**pas du tout... (sauf si vous insistez)**||


**Hey !**

`install.packages("rmarkdown")`

---

**Euuuh ! On commence par quoi ?**

- [ ] On construit le site puis on le déploie ?
- [X] On "déploie" le site avant de le construire ?


```

---

### `_site.yml`

```
name: "demoSiteWeb"  
author: "Cédric"  
output_dir: "docs"  
output:  
  html_document:  
navbar:  
  title: "Communauté R - Insee HdF"
  left:
    - text: ""
      href: index.html
    - text: "A propos"
      href: about.html

``` 

---


### **`.gitlab-ci.yml`**

Selon qu'on déploie son site sur [le gitlab externe (gitlab.com)](gitlab.com) ou le [gitlab interne Insee (https://gitlab.insee.fr)](https://gitlab.insee.fr), il faut adapter son fichier `.gitlab-ci.yml`

---

### **`.gitlab-ci`** du gitlab externe

```
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r docs/* .public
  - mv .public public
  artifacts:
    paths:
    - public
```

`docs` est le nom du dossier accueillant, notamment, les fichiers html résultant de la compilation des différents `.Rmd`.

Ce nom est défini par `output_dir: "docs"` du `_site.yml`

---

### **`.gitlab-ci`** du gitlab interne

On va ajouter les 3 lignes suivantes au début de son fichier

```
default:
  tags:
    - maven-jdk11
```

ce qui donne le `.gitlab-ci.yml` suivant :  

---

```
default:
  tags:
    - maven-jdk11

pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r docs/* .public
  - mv .public public
  artifacts:
    paths:
    - public
```

---

## Pour ajouter de nouvelles pages

- créer un fichier `exempleGitlab.Rmd` (par nouvelle page)  
  - qui génèrera un fichier `exempleGitlab.html`
- la spécifier dans le `_site.yml`  

---

### Le fichier **`_site.yml`**

```
name: "demoSiteWeb"  
author: "Cédric"  
output_dir: "docs"  
output:  
  html_document:  
    self_contained: no  
navbar:  
  title: "Ma Première Webpage"
  left:
    - text: ""
      href: index.html
    - text: "A propos"
      href: about.html
    - text: "Gitlab"
      href: exempleGitlab.html
    - text: "R Studio"
      href: rstudio.html
    - text: "Markdown"
      href: markdown.html
    - text: "style"
      href: style.html
    - text: "Codes"
      href: code.html
    - text: "Ressources"
      href: ressources.html

``` 

---

<details>
<summary markdown="span">**un exemple + complet de `_site.yml`**</summary>

```
name: "demoSiteWeb"
author: "Cédric"  
output:  
  html_document:  
    self_contained: no  
    theme: cosmo  
    css: style.css  
navbar:  
  title: "Communauté R - Insee HdF"
  type: inverse
  left:
    - text: ""
      icon: fa-home
      href: index.html
    - text: "Étapes"
      href: diaporama.html
    - text: "Intro"
      icon: fa-gear
      href: about.html
    - text: "Gitlab"
      href: gitlab.html
    - text: "R Studio"
      href: rstudio.html
    - text: "Insérer Code R"
      href: code.html
    - text: ""
      icon: fa-info
      menu:
        - text: "Menus"
        - text: "Tuto Markdown"
          href: markdown.html
        - text: "Tuto CSS"
          href: style.html
        - text: "Ressources"
          href: ressources.html
        - text: "Sorties diverses"
          href: OutputsR.html

  right:
    - icon: fa-question fa-lg
      href: https://gitlab.com/CTassart/demositeweb

```
</details>


# 4. Ajouter du style

Il y a différentes façons de "styliser" son site :

---

## 4.1 un style prédéfini

- En précisant un style prédéfini parmi [les thèmes `Bootstrap`](https://bootswatch.com/) suivants : `default`, `cerulean`, `journal`, `flatly`, `readable`, `spacelab`, `united`, `cosmo`, `lumen`, `paper`, `sandstone`, `simplex` ou `yeti`.  

- Ajouter la ligne `theme: cosmo` (par exemple) sous `html_document:` dans le fichier `_site.yml`.  

---

**Le fichier `_site.yml`**

```
name: "demoSiteWeb"  
author: "Cédric"  
output_dir: "docs"  
output:  
  html_document:  
    self_contained: no  
    theme: cosmo  
    css: style.css  
navbar:  
  title: "Ma Première Webpage"
  left:
    - text: "Home"
      href: index.html
    - text: "A propos"
      href: about.html
    - text: "Gitlab"
      href: gitlab.html
    - text: "R Studio"
      href: rstudio.html
    - text: "Markdown"
      href: markdown.html
    - text: "style"
      href: style.html
    - text: "Codes"
      href: code.html
    - text: "Ressources"
      href: ressources.html

``` 

---

## 4.2 une feuille de style personnalisée

En ajoutant une feuille de style personnalisée (un document qui détermine à quoi doit ressembler chaque élément de votre site Web)  

Ajouter la ligne `css: style.css` sous `html_document:` dans le fichier `_site.yml`.  

Créer un fichier nommé `style.css` et y ajouter vos styles personnalisés ([cf. exemple)](style.html).  

---

## 4.3 localement

En ajoutant une balise de style à la fin d'une page en particulier (cf exemple **`index.Rmd`**).  

---

## 4.4 Les ressources CSS

Le Web propose des milliers de guides sur CSS :  

- [**`codeacademy`**](https://www.codecademy.com/) propose d'excellents didacticiels interactifs  
- [**`W3Schools`**](https://www.w3schools.com/https://www.w3schools.com/)  
- [**`MDN`**](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics), [**`openClassrooms`**](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3)...

Cependant, les bases de CSS sont faciles à apprendre et il est préférable de commencer à jouer avec. Ajoutez le texte suivant à votre fichier `style.css` et effectuez un nouveau rendu du site Web à l'aide de `rmarkdown::render_site()`.   


# 5. Intégrer du code

![](../demositeweb_presentation/images/RStudio-Ball.png){ width=10%}

**Tout l'intérêt de créer un site web avec R (hormis la facilité) est... d'insérer du code R !**


## 51. Insérer du code R

Vous pouvez insérer un morceau de code R à l'aide des `chunks` R :  
- raccourci clavier `Ctrl` + `Alt` + `I`  
- barre d'outils RStudio

![](../demositeweb_presentation/images/10_chunk.png)

**Résultat**  
![](../demositeweb_presentation/images/10_chunk2.png)

## 52. Paramètres de `chunks`

Il existe un grand nombre d'options de paramétrage de ces chunks. 

- `eval`: faut-il évaluer un morceau de code ?  
- `echo`: Faut-il afficher le code  
- `warning`, `message`, et `error`: faut-il afficher les avertissements, messages et erreurs dans le document de sortie ?    

Pour en savoir + : [https://yihui.name/knitr/options](https://yihui.name/knitr/options).

![](../demositeweb_presentation/images/10_chunk3.png)

## 53. Exemples

**Il y a beaucoup de choses que vous pouvez faire dans un morceau de code : produire du texte, des tableaux ou des graphiques.**

---

### Un 1er exemple : une carte



---

### Un 2ème exemple : un tableau


<table class="table table-hover" style="width: auto !important; margin-left: auto; margin-right: auto;">
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Hello</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">World</div></th>
</tr>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;"> mpg </th>
   <th style="text-align:left;"> cyl </th>
   <th style="text-align:left;"> disp </th>
   <th style="text-align:left;"> hp </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Mazda RX4 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffd68b">21.0</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">160</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 32.84%">110</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mazda RX4 Wag </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffd68b">21.0</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">160</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 32.84%">110</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Datsun 710 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffcf78">22.8</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -ms-transform: rotate(180deg); -o-transform: rotate(180deg); transform: rotate(180deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">108</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 27.76%">93</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hornet 4 Drive </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffd487">21.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(240deg); -moz-transform: rotate(240deg); -ms-transform: rotate(240deg); -o-transform: rotate(240deg); transform: rotate(240deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">258</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 32.84%">110</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hornet Sportabout </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffdfa4">18.7</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(300deg); -moz-transform: rotate(300deg); -ms-transform: rotate(300deg); -o-transform: rotate(300deg); transform: rotate(300deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">360</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 52.24%">175</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Valiant </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffe1ab">18.1</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">225</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 31.34%">105</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Duster 360 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #fff0d4">14.3</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">360</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 73.13%">245</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 240D </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffc967">24.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -ms-transform: rotate(180deg); -o-transform: rotate(180deg); transform: rotate(180deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">146.7</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 18.51%">62</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 230 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffcf78">22.8</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(240deg); -moz-transform: rotate(240deg); -ms-transform: rotate(240deg); -o-transform: rotate(240deg); transform: rotate(240deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">140.8</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 28.36%">95</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 280 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffdd9f">19.2</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(300deg); -moz-transform: rotate(300deg); -ms-transform: rotate(300deg); -o-transform: rotate(300deg); transform: rotate(300deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">167.6</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 36.72%">123</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 280C </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffe2ae">17.8</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">167.6</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 36.72%">123</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 450SE </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffe8bd">16.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">275.8</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 53.73%">180</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 450SL </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffe4b4">17.3</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -ms-transform: rotate(180deg); -o-transform: rotate(180deg); transform: rotate(180deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">275.8</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 53.73%">180</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Merc 450SLC </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffecca">15.2</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(240deg); -moz-transform: rotate(240deg); -ms-transform: rotate(240deg); -o-transform: rotate(240deg); transform: rotate(240deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">275.8</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 53.73%">180</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Cadillac Fleetwood </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffffff">10.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(300deg); -moz-transform: rotate(300deg); -ms-transform: rotate(300deg); -o-transform: rotate(300deg); transform: rotate(300deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">472</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 61.19%">205</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Lincoln Continental </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffffff">10.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">460</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 64.18%">215</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Chrysler Imperial </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffeed0">14.7</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">440</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 68.66%">230</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Fiat 128 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffaa10">32.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -ms-transform: rotate(180deg); -o-transform: rotate(180deg); transform: rotate(180deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">78.7</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 19.70%">66</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Honda Civic </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffb225">30.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(240deg); -moz-transform: rotate(240deg); -ms-transform: rotate(240deg); -o-transform: rotate(240deg); transform: rotate(240deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">75.7</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 15.52%">52</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Toyota Corolla </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffa500">33.9</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(300deg); -moz-transform: rotate(300deg); -ms-transform: rotate(300deg); -o-transform: rotate(300deg); transform: rotate(300deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">71.1</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 19.40%">65</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Toyota Corona </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffd486">21.5</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">120.1</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 28.96%">97</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Dodge Challenger </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffebc7">15.5</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">318</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 44.78%">150</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> AMC Javelin </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffecca">15.2</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -ms-transform: rotate(180deg); -o-transform: rotate(180deg); transform: rotate(180deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">304</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 44.78%">150</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Camaro Z28 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #fff3df">13.3</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(240deg); -moz-transform: rotate(240deg); -ms-transform: rotate(240deg); -o-transform: rotate(240deg); transform: rotate(240deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">350</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 73.13%">245</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pontiac Firebird </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffdd9f">19.2</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(300deg); -moz-transform: rotate(300deg); -ms-transform: rotate(300deg); -o-transform: rotate(300deg); transform: rotate(300deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">400</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 52.24%">175</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Fiat X1-9 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffbe47">27.3</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">79</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 19.70%">66</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Porsche 914-2 </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffc355">26.0</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">120.3</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 27.16%">91</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Lotus Europa </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffb225">30.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -ms-transform: rotate(180deg); -o-transform: rotate(180deg); transform: rotate(180deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">95.1</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 33.73%">113</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ford Pantera L </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffeac4">15.8</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(240deg); -moz-transform: rotate(240deg); -ms-transform: rotate(240deg); -o-transform: rotate(240deg); transform: rotate(240deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">351</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 78.81%">264</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ferrari Dino </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffdb9a">19.7</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(300deg); -moz-transform: rotate(300deg); -ms-transform: rotate(300deg); -o-transform: rotate(300deg); transform: rotate(300deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">6</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">145</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 52.24%">175</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Maserati Bora </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffedcd">15.0</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(60deg); -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">8</span></span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: red !important;">301</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 100.00%">335</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Volvo 142E </td>
   <td style="text-align:left;"> <span style="display: block; padding: 0 4px; border-radius: 4px; background-color: #ffd487">21.4</span> </td>
   <td style="text-align:left;"> <span style="-webkit-transform: rotate(120deg); -moz-transform: rotate(120deg); -ms-transform: rotate(120deg); -o-transform: rotate(120deg); transform: rotate(120deg); display: inline-block; "><span style="     color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: red !important;text-align: center;">4</span></span> </td>
   <td style="text-align:left;"> <span style="  font-style: italic;   color: green !important;">121</span> </td>
   <td style="text-align:left;width: 3cm; "> <span style="display: inline-block; direction: rtl; unicode-bidi: plaintext; border-radius: 4px; padding-right: 2px; background-color: lightgreen; width: 32.54%">109</span> </td>
  </tr>
</tbody>
</table>

---


### Un 3ème exemple : un tableau

<table class="table table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Demo kableExtra pour Communauté R</caption>
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Group 1</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Group 2<sup>a</sup></div></th>
</tr>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:right;"> mpg </th>
   <th style="text-align:right;"> cyl </th>
   <th style="text-align:right;"> disp </th>
   <th style="text-align:right;"> hp </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Mazda RX4 </td>
   <td style="text-align:right;"> 21.0 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 110 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mazda RX4 Wag </td>
   <td style="text-align:right;"> 21.0 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 110 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Datsun 710 </td>
   <td style="text-align:right;"> 22.8 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 108 </td>
   <td style="text-align:right;"> 93 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hornet 4 Drive </td>
   <td style="text-align:right;"> 21.4 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 258 </td>
   <td style="text-align:right;"> 110 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hornet Sportabout </td>
   <td style="text-align:right;"> 18.7 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 360 </td>
   <td style="text-align:right;"> 175 </td>
  </tr>
</tbody>
<tfoot>
<tr>
<td style = 'padding: 0; border:0;' colspan='100%'><sup>a</sup> pied de page : 12 Octobre 2021</td>
</tr>
</tfoot>
</table>

---

### Un dernier exemple : un graph

```
library(viridis)
library(ggplot2)
library(doremifasolData)
data_iris_paris_2017 <- doremifasolData::data_iris_paris_2017
ggplot2::theme_set(theme_minimal())

ggplot(data_iris_paris_2017) + 
  geom_point(aes(x = niveau_vie_median, y = taux_chomage, color = part_cadres)) +
  scale_y_continuous(labels = scales::percent) +
  scale_color_viridis(labels = scales::percent) +
  guides(color=guide_colorbar(direction="horizontal",
                           title.position="top",
                           label.position="bottom")) +
  theme(legend.position = "bottom")
```

---

![](PresDemoSiteWeb_files/figure-html/pressure-1.png)

Cet exemple est tiré de la documentation [utilitR](https://www.book.utilitr.org/ggplot2.html)


# 6. Générer les pages du sites (localement)

- **2 méthodes :**  
  - Dans la console : exécuter **`"rmarkdown::render_site()"`** afin de générer (localement) toutes les pages du site web.  
  - ou `Build` / `Build Website`  
- Le bouton **`"knit"`** ne génèrera que la page "active" i.e. celle sur laquelle vous êtes positionné·e.

---

![](../demositeweb_presentation/images/8_build.png)

---

Les fichiers **`markdown`** seront transformés en HTML.  
    -  les `Rmd` commençant par "_" ne sont pas compilés.  
    -  Les `HTML` générés et tous les fichiers de support (par exemple `CSS` et `JavaScript`) sont copiés dans un répertoire de sortie (`docs`).  

# 7. Pousser son projet sur son Repo (`Git`)

---

C'est toujours la même chanson :  
- je modifie mes fichiers, je `commit` très régulièrement avec un message explicite, je `push` régulièrement.

---

![](../demositeweb_presentation/images/6_git_1.png)

---

![](../demositeweb_presentation/images/6_git_2.png)

---

![](../demositeweb_presentation/images/6_git_3.png)


[Pour + de détails, voir la formations aux outils collaboratifs de l'Insee](https://linogaliana.gitlab.io/collaboratif/git.html) et ou les ressources proposées dans [l'onglet Ressources](ressources.html)


# 8. gitlab Pages

---

Une fois votre travail "pushé" sur votre repo, vous trouverez l'adresse de votre site sur votre `gitlab pages`

![](../demositeweb_presentation/images/7_gitlabPages.png)

---

**ça marche aussi avec `Github` !**

- ***GitHub***
    - Avoir un compte `Github`
    - Créer un référentiel `Github` ("Repo")

---

Pour utiliser un référentiel GitHub en tant que site Web, vous devez définir :

- le répertoire source la première fois que vous ajoutez des fichiers au répertoire (ici, `docs`].  
  - Accédez au site Web de votre référentiel GitHub (par exemple, https://github.com/username/mywebpage)  
- Cliquez sur l'onglet `Settings` et faites défiler jusqu'à `GitHub Pages`  
- Sous **Source**, choisissez `master branch /docs folder` et cliquez **Save**

---

![](../demositeweb_presentation/images/7_githubPages.png)


<style>

h1{
  background-color: steelBlue;
}

h2{
  background-color: powderBlue;
}

h3{
  background-color: lightCyan;
}
summary {
    background-color: lightSteelBlue;
    color: white;
    border-style: double;
    border-color: white;
    text-align: left;
    font-size: 2em;
    line-height: 1;
    width: 90%;
    margin-left: 30px;


}

p{
text-align: left;
}

code{
color: darkBlue;
}

</style>



